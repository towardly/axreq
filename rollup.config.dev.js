/**
 * Created by haoran.shu on 2018/6/28 17:36.
 */
const path = require('path');

import typescript from 'rollup-plugin-typescript2';

export default {
  input: 'src/axreq.ts',
  output: {
    file: './dist/axreq.js',
    format: 'umd',
    name: "axreq",
    sourcemap: true,
    sourcemapFile: path.join(__dirname, "dist")
  },
  plugins: [
    typescript({
      tsconfig: path.join(__dirname, "tsconfig_umd.json")
    })
  ]
};
