import { serialize, post, get, json } from "../src/axreq";
import { expect } from 'chai';
import { useFakeXMLHttpRequest, fake } from 'sinon';


describe("axreq", () => {

  let xhr: any, requests:any;

  before(() => {
    xhr = useFakeXMLHttpRequest();
    requests = [];
    xhr.onCreate = function (req: any) { requests.push(req); };
  });

  after(function () {
    // Like before we must clean up when tampering with globals.
    xhr.restore();
  });


  it("serialize from HTMLElement", () => {
    let $form = document.createElement('form');
    $form.innerHTML = '<input type="text" name="username" value="admin"><input type="password" name="password" value="123456">';
    let s = serialize($form);
    expect(s).to.equal('username=admin&password=123456');
  });

  it('serialize json object', () => {
    let data = {username:'admin', 'password': '123456'};
    let s = serialize(data);
    expect(s).to.equal('username=admin&password=123456');
  });

  it('form get', function (done) {

    get('/request', 'username=admin&password=123456', fake());

    requests[requests.length - 1].respond(200, { 'Content-Type': 'text/plain' }, 'success');

    expect(requests[requests.length - 1].url).to.equal('/request?username=admin&password=123456');
    expect(requests[requests.length - 1].method).to.equal('GET');

    done();
  });

  it('form post string', function (done) {

    post('/request', 'username=admin&password=123456', fake());

    requests[requests.length - 1].respond(200, { 'Content-Type': 'text/plain' }, 'success');

    expect(requests[requests.length - 1].method).to.equal('POST');
    expect(requests[requests.length - 1].requestHeaders['Content-Type'].includes('urlencoded')).to.be.true;
    expect(requests[requests.length - 1].requestBody).to.equal('username=admin&password=123456');

    done();
  });

  it('form post object', function (done) {

    post('/request', {username: 'admin',password:'123456'}, fake());

    requests[requests.length - 1].respond(200, { 'Content-Type': 'text/plain' }, 'success');

    expect(requests[requests.length - 1].method).to.equal('POST');
    expect(requests[requests.length - 1].requestHeaders['Content-Type'].includes('urlencoded')).to.be.true;
    expect(requests[requests.length - 1].requestBody).to.equal('username=admin&password=123456');

    done();
  });

  it('form json urlencoded', function (done) {

    json('/request', {username: 'admin',password:'123456'}, 'POST', fake());

    requests[requests.length - 1].respond(200, { 'Content-Type': 'application/json' }, JSON.stringify({code:1}));

    expect(requests[requests.length - 1].method).to.equal('POST');
    expect(requests[requests.length - 1].requestHeaders['Content-Type'].includes('urlencoded')).to.be.true;
    expect(requests[requests.length - 1].responseHeaders['Content-Type'].includes('json')).to.be.true;
    expect(requests[requests.length - 1].requestBody).to.equal('username=admin&password=123456');

    done();
  });

  it('form json', function (done) {

    json('/request', {username: 'admin',password:'123456'}, 'POST', true, fake());

    requests[requests.length - 1].respond(200, { 'Content-Type': 'application/json' }, JSON.stringify({code:1}));

    expect(requests[requests.length - 1].method).to.equal('POST');
    expect(requests[requests.length - 1].requestHeaders['Content-Type'].includes('json')).to.be.true;
    expect(requests[requests.length - 1].responseHeaders['Content-Type'].includes('json')).to.be.true;
    expect(requests[requests.length - 1].requestBody).to.equal('{"username":"admin","password":"123456"}');

    done();
  });
});
