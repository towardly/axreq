interface ParamSetting {
    url: string;
    [propName: string]: any;
}
declare const _default: {
    serialize: (o: any) => string;
    request: (setting: ParamSetting, cb: (e: object | null, res: object | null) => void) => void;
    get: (url: string, ...restParams: any[]) => void;
    post: (url: string, ...restParams: any[]) => void;
    json: (url: string, ...restParams: any[]) => void;
};
export default _default;
