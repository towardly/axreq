declare const SCRIPTTYPERE: any;
declare const XMLTYPERE: any;
declare const JSONTYPE: string;
declare const HTMLTYPE: string;
/**
 * 进行对象复制
 */
declare function extend(...restParams: any[]): any;
/**
 * 根据 mime 匹配期望的数据类型
 * @param {string} mime text/plain;charset=utf-8
 * @returns {string} html, json, script, xml, text, 默认 text
 */
declare function mimeToResponseType(mime: string): string;
/**
 * 是否是原始对象:
 *  1. Object
 *  2. Not Null
 *  3. Not window or dom(bom)
 * @param obj
 * @returns {boolean} true -- 是原始对象
 */
declare function isPlainObject(obj: any): boolean;
declare function empty(): void;
interface ParamSetting {
    url: string;
    [propName: string]: any;
}
declare function serializeData(a: any, add: any): void;
declare function serializeForm(form: any, add: any): void;
export declare let serialize: (o: any) => string;
export declare let request: (setting: ParamSetting, cb: (e: object | null, res: object | null) => void) => void;
export declare let getPost: (url: string, method: string, ...restParams: any[]) => void;
export declare let get: (url: string, ...restParams: any[]) => void;
export declare let post: (url: string, ...restParams: any[]) => void;
export declare let json: (url: string, ...restParams: any[]) => void;
