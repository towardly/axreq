# axreq
> 注意：当前工程不再维护，推荐使用 [fetch](https://github.com/github/fetch "fetch")，可以参考我的另一个工程：[phax](https://github.com/DvShu/phax "phax")

js ajax 请求，基于 `XMLHttpRequest` 的封装。仓库地址：[码云](https://gitee.com/towardly/axreq "码云")
## 安装
1. npm(yarn)
```javascript
npm install axreq
yarn add axreq
```
2. 下载使用
到 `git` 仓库地址，下载下来，使用 dist 文件夹下的文件。
## 提供方法：
### axreq.serialize(object)
该方法用于将一个 `Form节点对象[HTMLElement]` 或者对象解析为 `urlencoded` 所需要的数据格式。
示例：
```javascript
<form id="form">
  <input type="text" name="username">
  <input type="text" name="password">
</form>

axreq.serialize(document.getElementById('form'))
// => username=xxx&password=xxx


axreq.serialize({ username:"admin",password: "123456" })
// => username=admin&password=123456
```
### axreq.request(config, callback);

congif 配置选项：
* url: 请求链接地址
* method: 请求方法，默认为 'GET', 允许的值有`GET`, `POST`
* processData: 提交数据之前，是否对数据进行操作；默认为 `true`, 将 `data` 转换为 `urlencoded` 的数据格式。
* contentType: `String | false`, 请求数据格式, 默认为 `application/x-www-form-urlencoded;charset=utf-8`；如果设置为 `false` 则不会设置 `content-type` 由系统自动生成(通常情况下是 `text/plain;charset=UTF-8`；如果传递的 `data` 是 `FormData`，则系统会自动转换为 `multipart/form-data`)
* responseType: 期望的返回数据格式，如果为 `json` 则会将返回的数据转换为 `JSON` 格式(`JSON.parse`)
* data: 上传的数据, 允许的格式为 `JSON对象`、`From节点`以及一切`XMLHttpRequest send()` 所能接收的参数；如果为 `JSON对象` 或 `Form节点`，默认情况下会转换为 `urlencoded` 请求所需的格式；如果请求方法为 'GET' 方法会自动将数据拼接到 url 的后面。
* headers: 需要设置的请求头信息

callback: 标准的 `nodejs` 回调。(err, res)

### axreq.get(url, [data,] cb);
执行 *get* 请求; 可以把参数拼接在 url 上, 也可以通过 data 传递参数, 如果传了3个参数则，第2个参数为请求的数据，第三个参数为请求的回调(包含成功和错误)。如果只传了两个参数，这个时候分两种情况; 如果第2个参数为 *object* 或者 *string* [通过 typeof 判断], 则将第2个参数视为请求的数据, 这个时候就相当于没有传递回调; 如果第2个参数为 *function* 则视为有回调但没有请求参数。
### axreq.post(url, [data,] cb);

执行 *post* 请求。参数规则，跟 *get* 请求相同。

请求参数：
* url: 请求地址
* data: 请求参数，可以为 `null | string | object`
* cb: 遵循 *nodejs* 风格的标准的回调函数 `function(err, data)`

### axreq.json(url, cb);
### axreq.json(url, data, cb);
### axreq.json(url, data, method|paramJson, cb);
如果传递的第3个参数类型(typeof)为 `string` 则视为传递的 `method` 参数；如果传递的参数类型为 `boolean` 则视为传递的 `paramJson` 参数。
### axreq.json(url, data, method, paramJson, cb);

请求 json 格式的数据，并且会自动将返回的结果 JSON 对象(JSON.parse())

请求参数：
* method: `String`，请求方法, 默认为 `GET`
* paramJson: `boolean`, 传递的参数是否为 json 格式，默认为 false, 如果传递为 true, 则会将请求的数据格式(content-type)设置为*application/json;charset=utf-8*


## 同时支持浏览器端使用和ES模块方式使用
### ES使用
```javascript
import { ajax, get, post, json } from 'axreq'
```

### 浏览器使用
```javascript
<script src="dist/axreq.min.js"></script>
<script>
axreq.ajax();
axreq.get();
axreq.post();
axreq.json();
</script>
```
